//
//  ViewController.swift
//  Cool EventBrite
//
//  Created by Peter Leung on 20/1/2017.
//  Copyright © 2017 winandmac Media. All rights reserved.
//

import UIKit
import SwiftyJSON

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var getData = DataFactory()
    
    var returnedData: ReturnedData?
    var events: [Event]? {
        return self.returnedData?.events
    }
    
    @IBOutlet weak var eventTableView: UITableView!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.eventTableView.delegate = self
        self.eventTableView.dataSource = self
        
        getData.getPosts(inputURL: searchUrlString) { (returnedData) in
            self.indicatorView.stopAnimating()
            self.returnedData = returnedData
            self.reloadEventTable()
        }
    }
    
    func reloadEventTable() {
        self.eventTableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventCell", for: indexPath) as! CustomTableViewCell
        
        if let event = events?[indexPath.row] {
            let title = event.nameText ?? ""
            let date = event.startLocalTime ?? ""
            let imageLink = event.logoUrl ?? ""
            
            cell.configureCell(title: title, date: date, imageLink: imageLink)
        } else {
            cell.configureCell(title: "", date: "", imageLink: "")
        }
        
        return cell
    }
    
    //When someone clicked the item
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let event = events?[indexPath.row]
        self.performSegue(withIdentifier: "showDetails", sender: event)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetails" {
            let detailViewController = segue.destination as! DetailViewController
            detailViewController.event = sender as? Event
        }
    }
    
    
}

