//
//  jsonEngine.swift
//  Cool EventBrite
//
//  Created by Peter Leung on 20/1/2017.
//  Copyright © 2017 winandmac Media. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import UIKit

class DataFactory{
    func getPosts(inputURL: String, completionHandler: @escaping (ReturnedData) -> Void) {
        Alamofire.request(inputURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: [:]).responseJSON { (response) in
            guard let data = response.result.value else{
                print("Request failed with error")
                return
            }
            
            let jsonData = JSON(data)
            let returnedData = ReturnedData(json: jsonData)
            completionHandler(returnedData)
        }
    }
}
