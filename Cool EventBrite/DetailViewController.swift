//
//  DetailVC.swift
//  Cool EventBrite
//
//  Created by Peter Leung on 20/1/2017.
//  Copyright © 2017 winandmac Media. All rights reserved.
//

import UIKit
import WebKit
import SafariServices

class DetailViewController: UIViewController, WKUIDelegate, WKNavigationDelegate, UIScrollViewDelegate {
    
    @IBOutlet weak var wkViewContainer: UIView!
    var webView = WKWebView()
    
    var event: Event?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 1.000, green: 0.341, blue: 0.000, alpha: 1.00)
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Adding WKWebView
        let frame = CGRect(x: 0, y: 0, width: wkViewContainer.bounds.width, height: wkViewContainer.bounds.height)
        webView.frame = frame
        webView.configuration.allowsInlineMediaPlayback = true
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.clipsToBounds = true
        wkViewContainer.addSubview(webView)
        
        //Loading Content
        generateFinalContent()
    }
    
    func generateFinalContent() {
        if let finalContent = self.event?.descriptionHtml {
            webView.loadHTMLString(finalContent, baseURL: URL(string: baseUrlString))
        }
    }
    
    @IBAction func gotoEventbrite(_ sender: Any) {
        loadExternalSafariController(inputURL: self.event?.url)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .linkActivated {
            loadExternalSafariController(inputURL: navigationAction.request.url?.absoluteString)
            decisionHandler(.cancel)
        }
        
        decisionHandler(.allow)
    }
    
    func loadExternalSafariController(inputURL: String?){
        if let urlString = inputURL, let url = URL(string: urlString){
            let SafariViewController = SFSafariViewController(url: url, entersReaderIfAvailable: false)
            self.present(SafariViewController, animated: true, completion: nil)
        }
    }
    
}
