//
//  shadowView.swift
//  Cool EventBrite
//
//  Created by Peter Leung on 20/1/2017.
//  Copyright © 2017 winandmac Media. All rights reserved.
//

import UIKit

@IBDesignable

class ShadowView: UIView {
    let shadowColor: CGFloat = 157.0/255.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 2.0
        layer.shadowColor = UIColor(red: shadowColor, green: shadowColor, blue: shadowColor, alpha: 0.5).cgColor
        layer.shadowOpacity = 0.8
        layer.shadowRadius = 5.0
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
    }
}
