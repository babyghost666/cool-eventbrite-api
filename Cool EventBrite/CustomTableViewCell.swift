//
//  customTVC.swift
//  Cool EventBrite
//
//  Created by Peter Leung on 20/1/2017.
//  Copyright © 2017 winandmac Media. All rights reserved.
//

import UIKit
import Kingfisher

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var eventImageThumb: UIImageView!
    @IBOutlet weak var eventDateLabel: UILabel!
    @IBOutlet weak var eventTitleLabel: UILabel!

    func configureCell(title: String, date: String, imageLink: String) {
        eventTitleLabel.text = title
        eventDateLabel.text = date
        eventImageThumb.kf.setImage(with: URL(string: imageLink))
    }

}
