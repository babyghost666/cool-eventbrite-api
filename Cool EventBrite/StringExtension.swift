//
//  StringExtension.swift
//  Cool EventBrite
//
//  Created by Peter Leung on 27/1/2017.
//  Copyright © 2017 winandmac Media. All rights reserved.
//

import Foundation

extension String {
    
    func stringByConvertingEventBriteDate() -> String? {
        return self.stringByConvertingDateFormat(fromDateFormat: "yyyy-MM-dd'T'HH:mm:ss", toDateFormat: "E, MMM d, yyyy, h:mm a")
    }
    
    func stringByConvertingDateFormat(fromDateFormat: String, toDateFormat: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromDateFormat
        
        if let date = dateFormatter.date(from: self) {
            dateFormatter.dateFormat = toDateFormat
            let toDateString = dateFormatter.string(from: date)
            return toDateString
        } else {
            return nil
        }
    }
}
