//
//  ReturnedData.swift
//  Cool EventBrite
//
//  Created by Peter Leung on 27/1/2017.
//  Copyright © 2017 winandmac Media. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ReturnedData {
    
    //var pagination: Pagination? // Not going to handle in this demo
    var events: [Event]?
    
    init(json:JSON){
        self.events = [Event]()
        
        if let eventsJSON = json["events"].array {
            for eventJSON in eventsJSON {
                let event = Event(json: eventJSON)
                self.events?.append(event)
            }
        }
    }
}
