//
//  Event.swift
//  Cool EventBrite
//
//  Created by Peter Leung on 27/1/2017.
//  Copyright © 2017 winandmac Media. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Event {
    
    var nameText: String?
    var startLocalTime: String?
    var logoUrl: String?
    var descriptionHtml: String?
    var url: String?
    
    init(json: JSON){
        self.nameText = json["name"]["text"].string
        
        if let startLocalTime = json["start"]["local"].string,
            let convertedDateString = startLocalTime.stringByConvertingEventBriteDate() {
            self.startLocalTime = convertedDateString
        }
        
        self.logoUrl = json["logo"]["url"].string
        
        self.descriptionHtml = json["description"]["html"].string
        
        self.url = json["url"].string
    }
}
